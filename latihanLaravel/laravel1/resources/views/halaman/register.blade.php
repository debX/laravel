@extends('layout.master')
@section('title')
    Halaman Register
@endsection
@section('judul')
    Sign Up Form
@endsection
@section('content')
    <form action="/signup" method="post">
        @csrf

            <label>First name: </label><br>
            <input type="text" name="f_name" placeholder="First Name" /><br><br>


            <label>Last Name: </label><br>
            <input type="text" name="l_name" placeholder="Last Name" /><br><br>


            <label>Gender: </label><br>
            <input type="radio"/> Male <br>
            <input type="radio" /> Female <br><br>

            <label>Nationality: </label><br><br>
            <select name="nationality:"> 
                <option value="indonesia">Indonesia</option>
                <option value="United States">United States</option>
                <option value="Inggris">Inggris</option>
            </select><br><br>

            <label>Language Spoken: </label> <br><br>
            <input type="checkbox" name="language"> Bahasa Indonesia<br>
            <input type="checkbox" name="language"> English <br>
            <input type="checkbox" name="language"> Other <br><br>

            <label>Bio: </label><br><br>
            <textarea name="bio" rows="10" cols="30"></textarea><br><br>

            <input type="submit" value="Sign Up" />
    </form>
@endsection
