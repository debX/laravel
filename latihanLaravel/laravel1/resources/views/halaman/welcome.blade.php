@extends('layout.master')
@section('title')
    Halaman Selamat Datang
@endsection
@section('judul')
    Berhasil Bergabung
@endsection
@section('content')
    <h2>Selamat Datang {{$f_name}} {{$l_name}}</h2>
    <h3>Terima kasih telah bergabung di website Kami. Media belajar kita bersama!</h3>
@endsection