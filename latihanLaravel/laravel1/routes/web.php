<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('table', function(){
    return view('halaman.table');
});

Route::get('data-tables', function(){
    return view('halaman.data-tables');
});

Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@register');
Route::post('/signup', 'AuthController@welcome');

Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
//Menampilkan Semua Data
Route::get('/cast', 'CastController@index');
//Detail
Route::get('/cast/{cast_id}', 'CastController@show');
//Ke Form Edit
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//Update Data
Route::put('/cast/{cast_id}', 'CastController@update');
//Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy');