<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }

    public function welcome(Request $request){
        //dd($request->all());
        $f_name = $request['f_name'];
        $l_name = $request['l_name'];

        return view('halaman.welcome', compact('f_name', 'l_name'));
    }
}
