@extends('layout.master')
@section('title')
    Halaman List Pemeran
@endsection
@section('judul')
     Menampilkan Data
@endsection
@section('content')
<a href="/cast/create" class="btn btn-primary my-3"> Tambah Data </a>
<div class="table-responsive">
    <table class="table table-striped table-hover">
     <thead>
        <tr>
            <th class="col-1">No</th>
            <th class="col-2">Nama</th>
            <th class="col-2">Umur</th>
            <th class="col-3">Bio</th>
            <th class="col-3">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item -> nama}}</td>
                <td>{{$item -> umur}}</td>
                <td>{{$item -> bio}}</td>
                <td>
                    <form action="/cast/{{$item->id}}" method="post">
                        @csrf
                        @method('delete')
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-sm btn-warning">Ubah</a>
                        <a href="/cast/{{$item->id}}" class="btn btn-sm btn-secondary">Detail</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Hapus"/>
                    </form>
                </td>
            </tr>
        @empty
            <h2>Data Kosong</h2>
        @endforelse
    </tbody>
    </table>
</div>
@endsection