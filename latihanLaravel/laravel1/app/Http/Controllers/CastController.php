<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }
    public function store(Request $request){
        $request->validate(
        [
            'nama' => 'required',
            'umur' => 'required',
            'bio'  => 'required',
        ],
        [
            'nama.required' => 'Nama Tidak Tidak Boleh Kosong',
            'umur.required'  => 'Umur Tidak Boleh Kosong',
            'bio.required'  => 'Biodata Tidak Boleh Kosong',
        ]);
        DB::table('cast')->insert([
            'nama' => $request['nama'], 
            'umur' => $request['umur'],
            'bio'  => $request['bio']
            ]
        );
        return redirect('/cast');
    }
    public function index(){
        $cast =DB::table('cast')->get();
        return view('cast.index', compact('cast'));
    }
    public function show($detail_id){
        $cast = DB::table('cast')->where('id', $detail_id)->first();
        return view('cast.show', compact('cast'));
    }
    public function edit($edit_id){
        $cast = DB::table('cast')->where('id', $edit_id)->first();
        return view('cast.edit', compact('cast'));
    }
    public function update($id, Request $request){
        $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required',
                'bio'  => 'required',
            ],
            [
                'nama.required' => 'Nama Tidak Tidak Boleh Kosong',
                'umur.required'  => 'Umur Tidak Boleh Kosong',
                'bio.required'  => 'Biodata Tidak Boleh Kosong',
            ]);
            
            DB::table('cast')->where('id', $id)
                ->update(
                    [
                        'nama' => $request['nama'],
                        'umur' => $request['umur'],
                        'bio' => $request['bio'],
                    ]
        );
        return redirect('/cast');
    }
    public function destroy($delete_id){
        DB::table('cast')->where('id', '=', $delete_id)->delete();
        
        return redirect('/cast');
    }
}

