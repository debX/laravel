@extends('layout.master')
@section('title')
    Halaman Ubah
@endsection
@section('judul')
     Ubah Data Pemeran
@endsection
@section('content')
    <form class="form-horizontal" action="/cast/{{$cast->id}}" method="post">
        @csrf
        @method('put')
    <!--FORM NAMA-->
    <div class="form-group mb-3 row">
        <label class="col-sm-3 control-label">Nama :*</label>
        <div class="col-sm-3">
            <input type="text" value="{{$cast->nama}}" name="nama" class="form-control">
        </div>
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <!--FORM UMUR-->
    <div class="form-group mb-3 row">
        <label class="col-sm-3 control-label">Umur :*</label>
            <div class="col-sm-3">
                <input type="text" value="{{$cast->umur}}" name="umur" class="form-control">
            </div>
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <!--FORM BIO-->
    <div class="form-group mb-3 row">
        <label class="col-sm-3 control-label">Bio :*</label>
            <div class="col-sm-4">
                <textarea name="bio" class="form-control">{{$cast->bio}}</textarea>
            </div>
   </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

   <!--SUBMIT-->
   <div class="form-group mb-3 row">
    <label class="col-sm-3 control-label"></label>
        <div class="col-sm-6">
            <input type="submit" class="btn btn-sm btn-primary" value="Simpan" data-toggle="tooltip" title="Simpan">
        </div>
   </div>
</form>
@endsection