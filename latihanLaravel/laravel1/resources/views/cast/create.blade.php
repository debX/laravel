@extends('layout.master')
@section('title')
    Halaman Pemeran
@endsection
@section('judul')
     Tambah Pemeran
@endsection
@section('content')
    <form class="form-horizontal" action="/cast" method="post">
        @csrf
    <!--FORM NAMA-->
    <div class="form-group mb-3 row">
        <label class="col-sm-3 control-label">Nama :*</label>
        <div class="col-sm-3">
            <input type="text" name="nama" class="form-control" placeholder="Masukan Nama">
        </div>
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <!--FORM UMUR-->
    <div class="form-group mb-3 row">
        <label class="col-sm-3 control-label">Umur :*</label>
            <div class="col-sm-3">
                <input type="text" name="umur" class="form-control" placeholder="Masukan Umur">
            </div>
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <!--FORM BIO-->
    <div class="form-group mb-3 row">
        <label class="col-sm-3 control-label">Bio :*</label>
            <div class="col-sm-4">
                <textarea name="bio" class="form-control" placeholder="Biodata Tambahan"></textarea>
            </div>
   </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

   <!--SUBMIT-->
   <div class="form-group mb-3 row">
    <label class="col-sm-3 control-label"></label>
        <div class="col-sm-6">
            <input type="submit" class="btn btn-sm btn-primary" value="Simpan" data-toggle="tooltip" title="Simpan">
        </div>
   </div>
</form>
@endsection