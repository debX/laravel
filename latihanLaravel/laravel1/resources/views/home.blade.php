@extends('layout.master')
@section('title')
    Dashboard
@endsection
@section('judul')
    Media Online
@endsection
@section('content')
    <h3>Sosial media Developer</h3>
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>

    <div>
        <h3>Benefit Join di Media Online</h3>
        <ul>
            <li> Mendapatkan motivasi dari sesama developer  </li>
            <li> Sharing Knowledge </li>
            <li> Dibuat oleh calon web developer terbaik </li>
        </ul>
    </div>

    <div>
        <h3>Cara Bergabung ke media online: </h3>
        <ol>
            <li>Mengunjungi Website ini</li>
            <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
        </ol>
    </div>
@endsection
